const pretty_print = (data, {showHidden = false, depth = null, colors = true} = {}) => {
    return console.log(util.inspect(data, {showHidden, depth, colors}));
};
